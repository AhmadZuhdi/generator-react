var generators = require('yeoman-generator');
var fs = require('fs');
var rmrf = require('rimraf');

module.exports = generators.Base.extend({
  // note: arguments and options should be defined in the constructor.
  constructor: function () {
    generators.Base.apply(this, arguments);

    this.argument('ReducerName', { type: String, required: true });
  },

  generate() {

    var [reducerName, ...actions] = this._args;

    rmrf.sync(this.destinationPath(`${reducerName}Reducer.js`));

    // fs.mkdirSync(this.destinationPath(reducerName));

    this.fs.copyTpl(
        this.templatePath('reducer.ejs'),
        this.destinationPath(`${reducerName}Reducer.js`),
        { reducerName, actions }
    );
  }
});