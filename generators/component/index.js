var generators = require('yeoman-generator');
var fs = require('fs');
var rmrf = require('rimraf');

module.exports = generators.Base.extend({
  // note: arguments and options should be defined in the constructor.
  constructor: function () {
    generators.Base.apply(this, arguments);

    this.argument('ComponentName', { type: String, required: true });
  },

  generate() {

    var componentName = this._args[0];

    // rmrf.sync(this.destinationPath(componentName));

    fs.mkdirSync(this.destinationPath(componentName));

    this.fs.copyTpl(
        this.templatePath('Component'),
        this.destinationPath(`${componentName}/${componentName}.jsx`),
        {
          componentName: componentName
        }
    );

    // this.fs.copyTpl(
    //     this.templatePath('Style.js'),
    //     this.destinationPath(`${componentName}/Style.js`)
    // );
  }
});